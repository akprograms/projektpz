﻿using SerwisASG.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace SerwisASG.DAL
{
    public class ASGServiceContext : DbContext
    {
        public ASGServiceContext() : base("DefaultConnection"){}

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Event> Meetings { get; set; }
        public DbSet<ProfilePhoto> ProfilePhotos { get; set; }
        public DbSet<EventPhoto> EventPhotos { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<EventPlace> EventPlaces { get; set; }
        public DbSet<Point> Points { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Profile>().HasMany<Event>(e => e.Events).WithMany(p => p.Players);
            modelBuilder.Entity<Profile>().HasMany<Event>(e => e.OrganizedEvents).WithRequired(p => p.Organizator).WillCascadeOnDelete(false);    
        }
    }
}