﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SerwisASG.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SerwisASG.DAL
{
    public class ASGServiceInitializer : DropCreateDatabaseIfModelChanges<ASGServiceContext>
    {
        protected override void Seed(ASGServiceContext context)
        {
            var roleManager = new RoleManager<IdentityRole>(
                new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var userManager = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(new ApplicationDbContext()));

            roleManager.Create(new IdentityRole("Admin"));

            var user = new ApplicationUser { UserName = "admin@test.com" };
            string password = "ZAQ12wsx_123";

            userManager.Create(user, password);

            userManager.AddToRole(user.Id, "Admin");

            var user2 = new ApplicationUser { UserName = "admin2@test.com" };
            string password2 = "ZAQ12wsx_123";

            userManager.Create(user2, password2);

            userManager.AddToRole(user2.Id, "Admin");
            //Console.WriteLine("asdfasdf");

            var photos = new List<ProfilePhoto>
            {
                new ProfilePhoto { Name = "photo1", Path = "Photos/photo1.jpg", Desc = "Opis1" },
                new ProfilePhoto { Name = "photo2", Path = "Photos/photo2.jpg", Desc = "Opis2" }
            };
            photos.ForEach(p => context.ProfilePhotos.Add(p));
            context.SaveChanges();

            var equipment = new List<Equipment>
            {
                new Equipment { Type = EquipmentType.Weapon, Name = "G&P M4A1 RAS", Description = "420 FPS lufa Edgi" }, // NAME & DESCRIPTION !
                new Equipment { Type = EquipmentType.Gear, Name = "Condor RRV", Description = "CB"  }
            };
            equipment.ForEach(e => context.Equipment.Add(e));
            context.SaveChanges();

            var profiles = new List<Profile>
            {
                new Profile { Email = "user1@test.com", Login = "USer1", Telephone = 123456789, SwagPoints = 20, Town = "Town", Equipment = new List<Equipment>{ equipment[0] } }, // 
                new Profile { Email = "test@abc.com", Login = "TestUser", Telephone = 987654321, SwagPoints = 30, Town = "TestTown" },
                new Profile { Email = "user2@xyz.abc", Login = "Second", Telephone = 123987456, SwagPoints = 25, Town = "Here" }
            };
            profiles.ForEach(p => context.Profiles.Add(p));
            context.SaveChanges();

            var points = new List<Point>
            {
                new Point { Lat = "51.959056", Lng = "21.195528" },
                new Point { Lat = "51.959190", Lng = "21.195678" }, // gora prawo
                new Point { Lat = "51.958906", Lng = "21.195678" }, // dol prawo
                new Point { Lat = "51.958906", Lng = "21.195378" }, // dol lewo
                new Point { Lat = "51.959190", Lng = "21.195378" } // gora lewo
            };
            points.ForEach(p => context.Points.Add(p));
            context.SaveChanges();

            var eventPlaces = new List<EventPlace>
            {
                new EventPlace { Name = "MK", MapZoom = "14", PlacePoint = points[0], PlaceZone = new List<Point> { points[1], points[2], points[3], points[4] } }
            };
            eventPlaces.ForEach(ep => context.EventPlaces.Add(ep));
            context.SaveChanges();

            var meetings = new List<Event>
            {
                new Event { Organizator = profiles[0], DateTime = DateTime.Parse("2016-04-23"), Description = "Sobotnie luźne spotkanie W2" + 
                "<br>Start godzina 10:00" +
                "<br>Ochrona oczu (całkowity zakaz zdejmowania na polu gry)" +
                "<br>Pomarańczowa Kamizelka (Jako oznaczenie \"TRUPA\" pomarańczowa nie zielona czarna czy różowa )" +
                "\nRepiliki RAM i Mancraft nie biorą udziału w grze.(Tippmann, Polarstar, Volwerin mogą uczestniczyć w grze)" +
                "\nMaksymalna moc dla replik CQB 0-350 FPS MAX 1,14J" +
                "\nMaksymalna moc dla replik w trybie Full Auto 0-450 FPS MAX 1,88J" +
                "\nMaksymalna moc dla replik w trybie Semi 450-600 FPS MAX 3,34J" +
                "\nRepliki z prędkością początkową większą od 600 FPS nie biorą udziału w zabawie." +
                "\nOsoby niepełnoletnie bawią się z nami tylko pod opieką opiekuna prawnego." +
                "\nOsoby nie spełniające naszych wymogów mogą nie zostać dopuszczone do gry.", EventPlace = eventPlaces[0] } // Place = "MK",  }
            };
            meetings.ForEach(m => context.Meetings.Add(m));
            context.SaveChanges();
        }
    }
}