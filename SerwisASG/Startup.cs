﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SerwisASG.Startup))]
namespace SerwisASG
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
