﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerwisASG.Models
{
    public class FullEventModel
    {
        public Event Event { get; set; }
        public EventPlace EventPlace { get; set; }
        public Point PlacePoint { get; set; }
        public ICollection<Point> PlaceZone { get; set; }
    }
}
