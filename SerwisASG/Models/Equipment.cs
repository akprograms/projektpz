﻿using System.Collections.Generic;

namespace SerwisASG.Models
{
    public class Equipment
    {
        public int ID { get; set; }
        public EquipmentType Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Profile> Owners { get; set; }
    }

    public enum EquipmentType
    {
        Weapon, Gear
    }
}