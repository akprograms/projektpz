﻿namespace SerwisASG.Models
{
    public class ProfilePhoto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Desc { get; set; }

        public virtual Profile Profile { get; set; }
        
    }
}