﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerwisASG.Models
{
    public class Point
    {
        public int ID { get; set; }
        public String Lat { get; set; }
        public String Lng { get; set; }

        public virtual EventPlace EventPlace { get; set; }

    }
}
