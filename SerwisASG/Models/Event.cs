﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SerwisASG.Models
{
    public class Event
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Data")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }
        public int ProfileID { get; set; } // organizator

        public String Description { get; set; }

        [Display(Name = "Miejsce spotkania")]
        public virtual EventPlace EventPlace { get; set; }

        public virtual ICollection<Profile> Players { get; set; }
        public virtual Profile Organizator { get; set; }
        public virtual ICollection<EventPhoto> Photos { get; set; }

    }
}