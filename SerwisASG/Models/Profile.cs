﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SerwisASG.Models
{
    public class Profile
    {
        
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Login nie może być dłuższy niz 20 znaków.")]
        public String Login { get; set; }
        public String Email { get; set; }
        public int Telephone { get; set; }
        public String Town { get; set; }
        public String About { get; set; }
        public int SwagPoints { get; set; }

        public virtual ICollection<ProfilePhoto> Photos { get; set; }
        public virtual ICollection<Equipment> Equipment { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<Event> OrganizedEvents { get; set; }

    }
}