﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SerwisASG.Models
{
    public class EventPhoto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; } // zamiast sciezki opis

        public virtual Event Event { get; set; }

    }
}