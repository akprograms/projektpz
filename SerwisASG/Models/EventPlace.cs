﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerwisASG.Models
{
    public class EventPlace
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String MapZoom { get; set; }

        [Display(Name = "Współrzędne")]
        public virtual Point PlacePoint { get; set; }
        
        public virtual ICollection<Point> PlaceZone { get; set; }
    }
}

// 51.959056, 21.195528 MK
