﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SerwisASG.ViewModels
{
    public class TownGroup
    {
        public string Town { get; set; }

        public int ProfileCount { get; set; }
    }
}