﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using SerwisASG.DAL;
using System.Data.Entity.Core.Objects;

namespace SerwisASG.WebServices
{
    /// <summary>
    /// Summary description for EventsWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EventsWebService : System.Web.Services.WebService
    {
        private ASGServiceContext db = new ASGServiceContext();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public String getEvents()
        {
            var json = "";

            //var events = from result in db.Meetings
            //             where result.ID.ToString() == eventID
            //             select result.DateTime;

            var events = db.Meetings.Select(e => new {
                title = e.EventPlace.Name,
                start = e.DateTime,
                end = EntityFunctions.AddHours(e.DateTime, 6), //new DateTime(e.DateTime.ToBinary()).AddHours(6).ToString(), //e.DateTime.AddHours(6).ToString()
                id = e.ID
            });

            JavaScriptSerializer jss = new JavaScriptSerializer();

            json = jss.Serialize(events);
            return json;
        }
    }
}
