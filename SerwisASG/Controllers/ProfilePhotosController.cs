﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SerwisASG.DAL;
using SerwisASG.Models;

namespace SerwisASG.Controllers
{
    public class ProfilePhotosController : Controller
    {
        private ASGServiceContext db = new ASGServiceContext();

        // GET: ProfilePhotos
        public ActionResult Index()
        {
            return View(db.ProfilePhotos.ToList());
        }

        // GET: ProfilePhotos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProfilePhoto profilePhoto = db.ProfilePhotos.Find(id);
            if (profilePhoto == null)
            {
                return HttpNotFound();
            }
            return View(profilePhoto);
        }

        // GET: ProfilePhotos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProfilePhotos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Path,Desc")] ProfilePhoto profilePhoto)
        {
            if (ModelState.IsValid)
            {
                db.ProfilePhotos.Add(profilePhoto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(profilePhoto);
        }

        // GET: ProfilePhotos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProfilePhoto profilePhoto = db.ProfilePhotos.Find(id);
            if (profilePhoto == null)
            {
                return HttpNotFound();
            }
            return View(profilePhoto);
        }

        // POST: ProfilePhotos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Path,Desc")] ProfilePhoto profilePhoto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(profilePhoto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(profilePhoto);
        }

        // GET: ProfilePhotos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProfilePhoto profilePhoto = db.ProfilePhotos.Find(id);
            if (profilePhoto == null)
            {
                return HttpNotFound();
            }
            return View(profilePhoto);
        }

        // POST: ProfilePhotos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProfilePhoto profilePhoto = db.ProfilePhotos.Find(id);
            db.ProfilePhotos.Remove(profilePhoto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
