﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SerwisASG.DAL;
using SerwisASG.Models;

namespace SerwisASG.Controllers
{
    public class EventPhotosController : Controller
    {
        private ASGServiceContext db = new ASGServiceContext();

        // GET: EventPhotos
        public ActionResult Index()
        {
            return View(db.EventPhotos.ToList());
        }

        // GET: EventPhotos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventPhoto eventPhoto = db.EventPhotos.Find(id);
            if (eventPhoto == null)
            {
                return HttpNotFound();
            }
            return View(eventPhoto);
        }

        // GET: EventPhotos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EventPhotos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Path")] EventPhoto eventPhoto)
        {
            if (ModelState.IsValid)
            {
                db.EventPhotos.Add(eventPhoto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(eventPhoto);
        }

        // GET: EventPhotos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventPhoto eventPhoto = db.EventPhotos.Find(id);
            if (eventPhoto == null)
            {
                return HttpNotFound();
            }
            return View(eventPhoto);
        }

        // POST: EventPhotos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Path")] EventPhoto eventPhoto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(eventPhoto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(eventPhoto);
        }

        // GET: EventPhotos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EventPhoto eventPhoto = db.EventPhotos.Find(id);
            if (eventPhoto == null)
            {
                return HttpNotFound();
            }
            return View(eventPhoto);
        }

        // POST: EventPhotos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EventPhoto eventPhoto = db.EventPhotos.Find(id);
            db.EventPhotos.Remove(eventPhoto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
