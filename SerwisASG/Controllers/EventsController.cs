﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SerwisASG.DAL;
using SerwisASG.Models;

namespace SerwisASG.Controllers
{
    public class EventsController : Controller
    {
        private ASGServiceContext db = new ASGServiceContext();

        // GET: Events
        public ActionResult Index()
        {
            return View(db.Meetings.ToList());
        }

        // GET: Events/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Meetings.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // GET: Events/Create
        public ActionResult Create()
        {
            // var tuple = new Tuple<Event, EventPlace>(new Event(), new EventPlace());
            FullEventModel fullEventModel = new FullEventModel() { Event = new Event(), EventPlace = new EventPlace(), PlacePoint = new Point(), PlaceZone = null };
            return View(fullEventModel);
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FullEventModel fullEventModel, String zoneCoords)
        {

            if(ModelState.IsValid)
            {
                List<string> coordsList = zoneCoords.Split(',').ToList();

                List<Point> points = new List<Point>();
                foreach (var item in coordsList)
                {
                    string[] tmp = item.Split(';');
                    points.Add(new Point() { Lat = tmp[0], Lng = tmp[1] });
                }

                db.Points.Add(fullEventModel.PlacePoint);
                db.SaveChanges();
                fullEventModel.EventPlace.PlaceZone = points;
                fullEventModel.EventPlace.PlacePoint = fullEventModel.PlacePoint;
                db.EventPlaces.Add(fullEventModel.EventPlace);
                db.SaveChanges();
                fullEventModel.Event.EventPlace = fullEventModel.EventPlace;
                fullEventModel.Event.Organizator = db.Profiles.Single(p => p.ID == 1);
                db.Meetings.Add(fullEventModel.Event);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fullEventModel);
        }

        // GET: Events/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Meetings.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DateTime,ProfileID,Description")] Event @event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(@event).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(@event);
        }

        // GET: Events/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Event @event = db.Meetings.Find(id);
            if (@event == null)
            {
                return HttpNotFound();
            }
            return View(@event);
        }

        // POST: Events/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event @event = db.Meetings.Find(id);
            db.Meetings.Remove(@event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Events
        [Authorize]
        public ActionResult JoinEvent(int id)
        {
            Event @event = db.Meetings.Find(id);
            Profile profile = db.Profiles.Single(p => p.Email == User.Identity.Name);
            @event.Players.Add(profile); // db.Profiles.Single(p => p.Email.Equals(User.Identity.Name))
            db.Entry(@event).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
