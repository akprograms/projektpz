﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SerwisASG.DAL;
using SerwisASG.ViewModels;

namespace SerwisASG.Controllers
{
    public class HomeController : Controller
    {
        private ASGServiceContext db = new ASGServiceContext();
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult About()
        {
            IQueryable<TownGroup> data = from profile in db.Profiles
                                                   group profile by profile.Town into townGroup
                                                   select new TownGroup()
                                                   {
                                                       Town = townGroup.Key,
                                                       ProfileCount = townGroup.Count()
                                                   };
            return View(data.ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}